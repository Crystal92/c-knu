#include <iostream>
#include <ctime> //srand
#include <cstdlib>
#include <vector> //vector
#include <stdio.h>
#include <stdlib.h>
#include <string>

using namespace std;

/*
 * game_menu : menu
 * @param void
 */
void game_menu();
/*
 * random_number : menu
 * @param int* ptr, int length
 */
void random_number(int* ptr, int length);
/*
 * guess_start : menu
 * @param int* ptr, int* ptr2, int length, vector<int> v
 */
vector<int> guess_start(int* ptr, int* ptr2, int length, vector<int> v, vector<string>* v_ptr, 
            int all_correct_num, int right_position_num);
/*
 * review : menu
 * @param void
 */
void review(vector<int> v);

int main()
{
    //two array
    int random[5]={0,0,0,0,0};
    int input_number[5]={0,0,0,0,0};
    
    //two array's pointer
    int* ptr = random;
    int* ptr2 = input_number;
    
    //int length = sizeof(random)/sizeof(random[0]);    
    int length = 5;
    
    int menu_select = 0;
    
    //history save array
    
    //save vector
    vector<int> v;
    vector<string> v_string;
    
    vector<string>* v_ptr;

    string save_string; 
    
    int all_correct_num=0;
    int right_position_num=0;    

    
    //first rand number generate
    random_number(ptr, length);
    

    while(1){
        //introduce for game munu
        game_menu();
        
        //at final, this number is
        //should hide

        
        cout << ">> select num : " ;
        cin >> menu_select;
        
        if(menu_select==1)
            v=guess_start(ptr, ptr2, length, v, v_ptr, all_correct_num, right_position_num);
        else if(menu_select==2)
            review(v);
        else if(menu_select==3)
            random_number(ptr, length);
        else if(menu_select==4){
            cout << ">> game Quit ........" << endl;
                    //vector ouput

            break;        
        }
        else{
            cout << ">> select ERROR !!!!!! " << endl;
        }
    }

    

    
    return 0;
}

/*
 * first game menu
 * @param void
 */

void game_menu(){
    cout << "----------------------------------"<< endl;    
    cout << "      The Code Breaker Game    " << endl;
    cout << "  1 : Guess " << endl;
    cout << "  2 : Review guesses " << endl;
    cout << "  3 : New game " << endl;
    cout << "  4 : Quit game " << endl;
    cout << "----------------------------------"<< endl;
    
}

/* new game : new random seacret 5 numbers
 * @param   int* ptr    :   array's pointer
            int length  :   array's length
 */
void random_number(int* ptr, int length){
    
    //random srand()
    srand( (unsigned int)time(NULL));
    
    //1-9 number random generate !
    for(int i=0; i<length; i++){
        ptr[i] = ( rand()%9 )+1;  
    }
    
    // //random number output test
    // for(int j=0; j<length; j++){
    //     cout << "element " << j << " : " << ptr[j] << endl;
    // } 
    
    cout << ">> new 5 numbers is generated" << endl;
    cout << endl;
}

vector<int> guess_start(int* ptr, int* ptr2, int length, vector<int> v, vector<string>* v_ptr, 
            int all_correct_num, int right_position_num){
    
    // the game chance is "12"
    // guess number is 1 -> 12, if 12 game is overed.
    int chance_num=12;
    int guess_num=0;
    
    // compare variable
    bool correct = false;

    
    // game result
    bool game_result = false;
    
    // game status
    char game_status ;
    
    string ppp[5];   

    //cout << ">> chance : " << chance_num << endl;
    cout << ">> guess the 5 numbers " << endl;
    cout << "-----------------------------" <<endl;
    
    
    //ptr : hiding numbers /////////////////
    // cout << ">> ";
    // for(int j=0; j<length; j++){
    //     cout <<  ptr[j] ;
    //     cout << " ";
    // }
    // cout << endl;
    //////////////////////////////////////////
    
    //input your test numbers
    //5 numbers between "space "
    for(int i=0; i<chance_num; i++){
        
        all_correct_num = 0;

        cout << ">> GUESS " << i+1 <<endl;
        
        //continue
        cin >> ptr2[0] >> ptr2[1] >> ptr2[2] >> ptr2[3] >> ptr2[4] ;
        guess_num = (i+1);
        
    
        //vector
        v.push_back(ptr2[0]);
        v.push_back(ptr2[1]);   
        v.push_back(ptr2[2]);  
        v.push_back(ptr2[3]);  
        v.push_back(ptr2[4]);  
        
        //compare
        for(int j=0; j<5; j++){
            for(int k=0; k<5; k++){
                if(ptr[j]==ptr2[k]){
                    
                    //for test ouput
                    //cout << ptr[j] << " " << ptr2[k] << endl;
                    
                    correct = true;
                    ptr2[k]=-1;
                    all_correct_num += 1;
                    
                    if(j==k)
                        // same number, and same position
                        right_position_num +=1;
                    break;
                }
            
                
            }// for loop : k
        }// for loop : j
        
        
        cout << "Result = " << all_correct_num << " numbers correct. ";
        cout << "  " << right_position_num << "  in right position. " << endl;
        
        
        
        
        // //vector ouput
        // for (int i=0; i<v.size(); i++) {
        //     cout << v[i] << " " ; 
        // }
        // cout << endl;
        // i want to return this vector,
        // because for  review function
        
        //game success
        if(all_correct_num==5 && right_position_num==5){
            game_status==true;
            cout << "--------------------------------" << endl;
            cout << ">> GAME SUCCESS !! XD " << endl;
            cout << ">> Count  : " << guess_num << endl;            
            break; // loop exit            
        }
        
         // for next, initial variable
        all_correct_num = 0;
        right_position_num = 0;        
        
        cout << ">> do you want to give up? (y/n) : " ;
        cin >> game_status;
        
        //game give up
        if(game_status=='y' || game_status=='Y')
            break; 
        else if(game_status=='n' || game_status=='N')
            cout << endl;
        else
            cout << ">> INPUT ERROR" << endl;
            
        // if game fail
        //     if(game_result==false){
        //     cout << "--------------------------------" << endl;
        //     cout << ">> Result : GAME FAIL ! " << endl;
        //     break;
        // }    
        
    } // for loop 12 chance
    

    return v;
    
}

void review(vector<int> v){
    cout << ">> your game history " << endl;   
    
    if(v.size() > 0){
        //vector ouput
        for (int i=0; i<v.size(); i++) {
        cout << v[i] << " " ;
        } // this for loop error, 
        cout << endl;       
    }else
        cout << ">> NO HISTROY." << endl;
        cout << endl;

}
