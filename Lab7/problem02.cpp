#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>

using namespace std;

int main()
{
    int count =0 ;
    
    int count_i=0 ;
    int count_e=0 ;
    int count_a=0 ;
    int count_o=0 ;
    int count_u=0 ;
    
    cout << "enter a word :" ;
    //string word;
    string sentence;
    
    //cin >> word;
    getline(cin, sentence);
    
    for(int i=0 ; i<sentence.length() ; i++){
        string letter = sentence.substr(i, 1);
        
        if(letter == "i"){
            count_i ++;
        }else if(letter == "a"){
            count_a ++;
        }else if(letter == "e"){
            count_e ++;
        }else if(letter == "o"){
            count_o ++;
        }else if(letter == "u"){
            count_u ++;
        }
        
    }
    
    cout << "There was i : " << count_i <<  endl;
    cout << "There was e : " << count_e <<  endl;
    cout << "There was u : " << count_u <<  endl;
    cout << "There was a : " << count_a <<  endl;
    cout << "There was o : " << count_o <<  endl;
    
    int sum =count_a+count_o+count_u+count_e+count_i;
    cout << "There was all word : " << sum <<  endl;    
    
    return 0;
}