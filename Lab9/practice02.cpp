//array practice
//parameter <- array pointer
//sort, reverse sort

//http://mwultong.blogspot.com/2006/11/c-array-pass-to-function.html
//http://gshscs.tistory.com/11

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cstring>
#include <algorithm> //sort function

#include<functional> // down-sort

using namespace std;

void order_arrray(int* array, int size);
void reverse_order(int* array, int size);
void one_first_last(int* array, int size);

int main()
{
    int size=100;
    int array[size];
    int a;
    
    //random
    srand( (unsigned int)time(NULL));


    
    cout << "input want array size : " ;
    cin >> size;
    
    for(int i=0; i<size; i++){
        array[i] = ( rand()%1000 )+1;  
    }
    
     for(int j=0; j<size; j++){
        cout << "element " << j << " : " << array[j] << endl;
    }  
    
    // cout << "------------------------------" << endl;
    // cout << "a. Every element at an even index. " << endl;
    
    // for(int j=0; j<size; j++){
    //     if(j%2==0)
    //         cout << "element " << j << " : " << array[j] << endl;
    // } 
    // cout << "------------------------------" << endl; 
    // cout << "b. Every even element. " << endl;
    
    // for(int j=0; j<size; j++){
    //     if(array[j]%2==0)
    //         cout << "element " << j << " : " << array[j] << endl;
    // } 
    
    cout << "------------------------------" << endl;
    int* ptr = array;
    
    //first test for sort
    order_arrray(array, size);
    
    //reverse_order
    reverse_order(array, size);
    
    //one_first_last
    one_first_last(array, size);

    
    return 0;
}

void order_arrray(int* array, int size){
    //sort this array
    sort(array, array+size);   

    cout << " order_array " << endl;
    
    for(int j=0; j<size; j++){
            cout << "element " << j << " : " << array[j] << endl;
    }     
    cout << "------------------------------" << endl;    
}

void reverse_order(int* array, int size){
    //reverse_sort_ouput
    sort(array, array+size, greater<int>());     
    
    cout << " Reverse :: order_array " << endl;
    
    for(int j=0; j<size; j++){
            cout << "element " << j << " : " << array[j] << endl;
    }     
    cout << "------------------------------" << endl;    
    
}

void one_first_last(int* array, int size){
    
    cout << "first element : " << array[0] << endl;
    cout << "last element : " << array[size-1] <<endl;
}