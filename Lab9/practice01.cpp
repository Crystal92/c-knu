#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    const int CAPACITY = 10;
    int values[CAPACITY]; 
    bool found = false;
    int pos =0;
    int size =0;
    int input;
    
    cout << "enter a series of values with nQ to end " <<endl;
    
    //10 times input & enter
    while( size < CAPACITY ){
        cin >> input;
        if (cin.fail())
            { 
                break; 
            }
            values[size] = input ; // int -> int[array] input
            size++;
    }
    
    
    
    //sort this array
    sort(values, values+size);
    
    for (int i=0; i<size; i++){
        cout << "element " << i << " " <<values[i] <<endl;
    }
    
    int find_position;
    
    cout << "which find do you want a position? >> " ;
    cin >> find_position;
    
    while( pos < size && !found ){
    
        if(values[pos] == find_position){
            found = true;
            cout << "found at position " << pos <<endl;
        }
        else 
            pos ++;
    }
    
    int find_element;
    
    cout << "what find do you want element[?] >> " ;
    cin >> find_element;
    
    cout << "found at element : " << values[find_element] <<endl;    
    

    return 0;
}