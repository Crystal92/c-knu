#include <iostream>

using namespace std;

int main()
{
    int floor;
    int actual_floor;
    
    cout << "Floor:" ;
    cin >> floor;
    
    if(floor > 13){
        actual_floor = floor -1 ;
        cout << "Method 1: thr elevator will travel to the actual floor" <<
        actual_floor << endl;
    }
    else{
        actual_floor = floor;
        cout << "Method 1: thr elevator will travel to the actual floor" <<
        actual_floor << endl;        
    }
    
    actual_floor = floor;
    if( floor > 13){
        actual_floor --;
    }
    cout << "Method 2: thr elevator will travel to the actual floor" <<
    actual_floor << endl;
    
    actual_floor = floor;
    if( floor > 13){
        actual_floor --;
    }
    cout << "Method 3: thr elevator will travel to the actual floor" <<
    actual_floor << endl;   
    
    actual_floor = floor > 13 ? floor-1 : floor;
    
    return 0;
}