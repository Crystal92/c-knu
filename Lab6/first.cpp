#include <iostream>

using namespace std;

int main()
{
    //이자?
    cout << "interest rate" << endl;
    double rate;
    cin >> rate;
    
    //처음 돈?
    cout << "enter initial amount : " << endl;
    double initial_balance;
    cin >> initial_balance;
    
    //최종 금액 ?  아니면 년도?
    cout << "enter target : " <<endl;
    double target;
    cin >> target;
    
    //
    double balance = initial_balance ;
    
    
    // for(int i=0 ; i++ ; i<target){
    //     initial_balance = initial_balance + ( initial_balance * rate * 0.01 ) ;
    // }

    // while( initial_amount == target){
    //     initial_balance = initial_balance + ( initial_balance * rate * 0.01 ) ;
    // }
    
    int year=0;
    double interest=0;
    
    while(balance < target){
        year++;
        interest = balance * rate / 100;
        balance = balance + interest;
    }
    
    cout << "the investment doubled after " << year << "years" << endl;
    
    return 0;
}