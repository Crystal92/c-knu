#include <iostream>

using namespace std;

void temp_check(string sel_temp, double input_temp, int c_or_f){
    
    //after chage tempertaure
    double change_temp;
    change_temp = (input_temp*1.8)-32;
    
    //solid , liquid, gaseous at the sea level.    
    cout << "---sea level : -2(solid), -1~99(liquid) ,100(gaseous)-----" << endl;
    
    if(c_or_f == 1){
        
        if(input_temp<=-2)
            cout << ">> in your tempertaure, sea is solid ." <<endl;
        else if(input_temp <=99)
            cout << ">> in your tempertaure, sea is liquid ." <<endl;       
        else if(input_temp >=100)
            cout << ">> in your tempertaure, sea is gaseous ." <<endl;    
            
    }else if(c_or_f==0){
    
        cout << "Fahrenheit : " << input_temp << " -> celsius : " <<  change_temp << endl;          
        
        if(change_temp<=-2)
            cout << ">> in your tempertaure, sea is solid ." <<endl;
        else if(change_temp <=99)
            cout << ">> in your tempertaure, sea is liquid ." <<endl;       
        else if(change_temp >=100)
            cout << ">> in your tempertaure, sea is gaseous ." <<endl; 
    }
}

int main()
{
    string sel_temp;
    int c_or_f=0;
    double input_temp;
    //double change_temp;
    
    cout << "letter C for Celsius or F for Fahrenheit." << endl;
    cout << "select C or F  : " ;
    cin >> sel_temp;
    
    if(cin.fail()){
        cout << "input error " << endl;
        return 1;
    }
    
    if( (sel_temp=="c") || (sel_temp=="C")){
        
        //celsius on = 1
        c_or_f = 1;
        
        cout << "Celsius temp select" <<endl;
        
        cout << "input temperature : ";
        cin >> input_temp;
        
        if(cin.fail()){
            cout << "input_temperature is error " << endl;
            return 1;
        }
        
        //funcion
        temp_check(sel_temp, input_temp, c_or_f);
        
    }else if( (sel_temp=="F") || (sel_temp=="f") ){
        
        //Fahrenheit on = 0;
        c_or_f = 0;
        
        cout << "Fahrenheit temp select" <<endl;
        
        cout << "input temperature : ";
        cin >> input_temp;
        
            if(cin.fail()){
                cout << "input_temperature is error " << endl;
                return 1;
            }
            
            //funcion
            temp_check(sel_temp, input_temp, c_or_f);
        }
        
    // cout << " " << endl;          
    // cout << "your select : " << sel_temp << endl;  
    
    return 0;
}
