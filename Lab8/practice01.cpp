#include <iostream>

using namespace std;

int main()
{
    double s_length = 2;
    double v = s_length * s_length * s_length;
    
    cout << "a cube with side length 2 is " << v << endl;
    
    s_length = 10;
    double v1 = s_length* s_length * s_length ;
    cout << "a cube with side length 10 is " << v1 << endl;
    

    //learn the "function" ! 
    double result1 = cube_v(2);
    double result2 = cube_v(10);
    
    cout << "a cube with side length 2 is" << result1;
    cout << "a cube with side length 10 is" << result2;
    
    return 0;
}