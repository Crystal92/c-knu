#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cstring>

using namespace std;

/*
 *function : random palindrome generator
 *@param void
 *@return string
 */
string random_upper( );
string random_lower( );
string random_number( );

/*
 *function : checks_palindrome, if_ignore_select_case
 *@param void
 *@return void
 */
void checks_p();
void if_ignore_select_case();

/*
 *function : ignore_case
 *@param string input_sentence
 *@return bool
 */
bool ignore_case(string input_sentence);

/*
 *function : a rendom selecting ASCII word
 *@param int input_length 
 *@return string
 */
string random_select_ascii(int input_length );

/*
 *function : selecting ASCII word, then reverse
 *@param string string_return, int input_length
 *@return void
 */
void reverse_string(string string_return, int input_half);

///////////main function/////////////////////////////////////
int main()
{
    // every time loop
    // input number for selecting program
    // number "4"is program exit
    
    // variable input num
    int select_num;
    int input_length;
    
    string returned_sentence="";
    
    
    // infomation select program
    while(1){
    cout << "---------assignment--------------" << endl;
    cout << "1. random palindrome generator." << endl;
    cout << "2. checks palindrome" << endl;
    cout << "3. ignore case Uppercase, lowercase" << endl;
    cout << "4. exit" <<endl;
    cout << "-------input your selcet----------" << endl;    
    
    // input selcet program's number
    cout << ">> selcet program : ";
    cin >> select_num;
    
    //input error
    if(cin.fail()){
        cout << "select_num input ERROR !!" << endl;
        cout << "" << endl; 
        return -1;        
    }
    
    if(select_num>4 || select_num <=0){
        // no menu except
        cout << " your select number is EORROR !!" << endl;
        cout << "" << endl; 
        return -1;          
    }

            //select menu
            //user input nuber 1, 2, 3, 4
            //if not 1-4, there are error.
            if(select_num==1){
                returned_sentence = random_select_ascii(input_length);
                break;        
            }
            else if(select_num==2){
                checks_p();
                break;                  
            }
            else if (select_num==3){
                if_ignore_select_case();
                break;                
            }
            else if (select_num==4)
               break; // while loop finish
            // }
    
        // } //if(select_num != 4) finish
    } //while finish
   return 0;
}

/////////first function : random palindrome generator/////////////////////
string random_upper(){
    int a;
    string palindrome_upper ;
    
    // time seed
    //rand((unsigned int)time(NULL));

    // exchage 1 random_select -> upper case word 1
    a= (rand() % 26) + 65;
    // cout << a << " " <<endl;
    palindrome_upper=a;
    cout << palindrome_upper ; 
    
    return palindrome_upper;
    
}

////////////////////////////////////////////////////////////////////
string random_lower(){
    int a;
    string palindrome_low ;    
    
    // time
    //srand((unsigned int)time(NULL));

    // exchage 1 random_select -> lower case word 1
    a= (rand() % 26) + 97;
    //cout << a << " " <<endl;
    palindrome_low=a;
    cout << palindrome_low ; 
    
    return palindrome_low;
    
}

////////////////////////////////////////////////////////////////////
string random_number(){
    int a;
    string palindrome_num ;   

    // time
    // srand((unsigned int)time(NULL));

    a= (rand() % 10) + 48;
    // cout << a << " " <<endl;
    
    //number -> conver string
    palindrome_num=a;
    
    //output
    cout << palindrome_num ;
    
    return palindrome_num;

}

////////////////////////////////////////////////////////////////////
// function : checks palindrome
void checks_p(){
    
    //variable
    int input_length; //input sentence's length
    int half_length; //input sentence's half length
    string input_sentence=""; // string variable
    
    
    //while loop
    while(1){
        
        bool correct_palin_false = true; 
        
        cout << ">> [EXIT] is q  :" << endl;       
        cout << ">> Enter a palindrome :" ;
        cin >> input_sentence;
        
        ////first input error :: cin.fail
        if(cin.fail()){
            cout << "input error : input_sentence " << endl; 
        }else{
            input_length=input_sentence.length() ;
        }
        
        //if input is q -> while break.
        if(input_sentence == "q" || input_sentence=="Q"){
            break;
        }else if((input_length%2) != 0){
            cout << ">> input error : your sentence is not odd " << endl;   
        }else{
            
            int j=input_length-1;
            
            //if correct input_sentence
            for(int i=0; i<input_length/2; i++){
                    if(input_sentence[i] == input_sentence[j]){
                        cout << input_sentence[i];
                        cout << " " << input_sentence[j];
                        cout << " same! " << endl ;
                        j--;
                    }else if(input_sentence[i] != input_sentence[j]){
                        cout << input_sentence[i];
                        cout << " " << input_sentence[j];
                        cout << " false! " << endl;                        
                        correct_palin_false = false;
                        j--;
                        //if there is just one false, all sentence is "false".
                    }else{
                        cout << ".. error....." << endl;
                    }
            }//for loop end
            
            //out put
            if(correct_palin_false==false){
                cout << ">> your palindrome is FLASE  !! " << endl;            
            }else{
                cout << ">> This is a valid palindrome  !! " << endl;   
            }
        
        }//else end
        
        cout << "" << endl;
        
    }//while end
    
  cout << "-----------------------------------" << endl;   
}

////////////////////////if you select third program
//this function'll call.
void if_ignore_select_case(){
    
    //variable
    int input_length; //input sentence's length
    int half_length; //input sentence's half length
    string input_sentence=""; // string variable
    
    while(1){
        
        bool on_off = false ;
        bool correct_palin_false = true;        
        
        cout << ">> [EXIT] is q  :" << endl;       
        cout << ">> Enter a palindrome :" ;
        cin >> input_sentence;
        
        
        ////first input error :: cin.fail
        if(cin.fail()){
            cout << "input error : input_sentence " << endl; 
        }else{
            input_length=input_sentence.length() ;
        }
        
        //if input is q -> while break.
        if(input_sentence == "q" || input_sentence=="Q"){
            break;
        }else if((input_length%2) != 0){
            cout << ">> input error : your sentence is not odd " << endl;   
        }else{
            
            //////////////////ignore function call
            on_off = ignore_case(input_sentence);
            
            if(on_off==true){
  
                int j=input_length-1;
                //if correct input_sentence
                for(int i=0; i<input_length/2; i++){
                        
                        if(input_sentence[i] == input_sentence[j]){
                            cout << input_sentence[i];
                            cout << " " << input_sentence[j];
                            cout << " same! " << endl;
                            j--;
                            
                        }else if(input_sentence[i] == input_sentence[j]-32){
                            cout << input_sentence[i];
                            cout << " " << input_sentence[j];
                            cout << " same! " << endl;
                            j--;                            
                            
                        }else if(input_sentence[i] == input_sentence[j]+32){
                            cout << input_sentence[i];
                            cout << " " << input_sentence[j];
                            cout << " same! " << endl;
                            j--;                            
                        }else{
                            cout << input_sentence[i];
                            cout << " " << input_sentence[j];
                            cout << " false! " << endl;                        
                            correct_palin_false = false;
                            j--;
                            //if there is just one false, all sentence is "false".
                        }
                }//for loop end
            
                //out put
                if(correct_palin_false==false){
                    cout << ">> your palindrome is FLASE  !! " << endl;            
                }else{
                    cout << ">> This is a valid palindrome  !! " << endl;   
                }   
             
             //on_off_function if false   
            }else if(on_off==false){
                //not ignore
                //if correct input_sentence
                int j=input_length-1;
                //if correct input_sentence
                for(int i=0; i<input_length/2; i++){
                        
                        if(input_sentence[i] == input_sentence[j]){
                            cout << input_sentence[i];
                            cout << " " << input_sentence[j];
                            cout << " same! " << endl;
                            j--;
                        }else{
                            cout << input_sentence[i];
                            cout << " " << input_sentence[j];
                            cout << " false! " << endl;                        
                            correct_palin_false = false;
                            j--;
                            //if there is just one false, all sentence is "false".
                        } 
                }//for loop

            //out put
            if(correct_palin_false==false){
                cout << ">> so your palindrome is FLASE  !! " << endl;            
            }else{
                cout << ">> so This is a valid palindrome  !! " << endl;   
            }
        
        }//else end
        
        cout << "" << endl;
        
    }//while end
    
        cout << "-----------------------------------" << endl;   
    }

}
      
    
// function : ignore case Uppercase, lowercase checek
// on: ignore
// off : not ignore
bool ignore_case(string input_sentence){
    
    string ingre_onoff="";
    
    cout << " " << endl;
    
    cout << "Do you want to ignore case? : Y / N" << endl;
    cin >> ingre_onoff;
    
    //except
    if(cin.fail())
        cout << "input fail " <<endl;
    
    //if-else
    if(ingre_onoff=="y" || ingre_onoff=="Y"){
       //cout << ">> yes !" <<endl; 
       return true;
    }else if(ingre_onoff=="n" || ingre_onoff=="N"){
       //cout << ">> no !" <<endl; 
       return false;
    }else{
        cout << ">> input error ..... " <<endl;
        input_sentence = "q";
    }
}

// function : random select Uppercase, lowercase, number
string random_select_ascii(int input_length){
    
    // input length the random sentence
    cout << ">> input length : ";
    cin >> input_length; //-> user input_length
    
    // input error
    if(cin.fail()){
        cout << ">> input_length input ERROR !!" << endl; 
        cout << "" << endl; 
    }
        
    // palindrome is not odd.
    // so we need even length.
    // if not even, there are error code.
    if( (input_length%2) != 0){
        cout << ">> input_length IS ODD. we need even sentence !!" << endl; 
        cout << "" << endl;
    }   
    
    // variable;
    int random_select;
    // palindrome's half length
    int input_half = input_length/2;
    
    string string_return="";
    
    // time seed
    srand((unsigned int)time(NULL));
    
    for(int i=0; i<input_half; i++){

        random_select = rand() % 100000;
        
        // 0, 1, 2  select
        random_select = random_select % 3 ;
        
        //output
        //cout << random_select <<" " << endl;
        
        //if-else loop
        if(random_select == 0){
            string_return += random_number();
            //cout << string_return << endl;   
        }
        else if(random_select == 1){
            string_return += random_upper();
            //cout << string_return << endl; 
        }
        else if(random_select == 2){
            string_return += random_lower(); 
            //cout << string_return << endl;  
        }
        else{
            cout << "random_select ERROR" << endl;
        }
    }
    
    //if return each function, string_return have returned-sentences.
    //for return test
    //cout << "before : " << string_return << endl; 
    
    //string_return reverse
    reverse_string(string_return, input_half);
    
    return string_return;
        
}

//this function is reverse for "string_return"
void reverse_string(string string_return, int input_half){
    
    //variable
    string str = string_return;
    int length = input_half;
    
    cout << " " ;
    
    //for loop
    for(int i=length; i>=0; i--){
        cout << str[i] ; 
    }
    
    cout << "" << endl;
} 
