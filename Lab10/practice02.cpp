#include <iostream>

using namespace std;

int main()
{
    double harry_account = 0;
    double joint_account = 2000;
    
    double* account_pointer = &harry_account;
    
    *account_pointer = 1000; // initial deposit
    
    *account_pointer = *account_pointer -100 ; // withdraw
    
    
    cout << "balance : " << *account_pointer << endl;
    
    
    return 0;
}