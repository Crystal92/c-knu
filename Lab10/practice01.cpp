#include <iostream>

using namespace std;

int main()
{
    int myv=10;
    int another=20;
    int* ptr = &myv;
    
    cout << "myv : "<< myv << endl;
    cout << "another : " << another <<endl;
    cout << "adrress of another : " << &another << endl;
    cout << " " << endl;
    cout << "adrress of myv : " << &myv << endl;
    cout << "pointer : " << ptr << endl;
    //error : cout << "adrress of myv : " << *myv << endl; 
    cout << endl;
    cout << "pointer : " << *ptr <<endl;
    cout << endl;
    
    //2th
    ptr = &another;
    cout << "adrress of myv : " << &another << endl;
    cout << "pointer : " << ptr << endl;
    return 0;
}